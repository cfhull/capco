import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'capco-app';
  data: object = {};

  constructor(private service: ApiService) {}

  ngOnInit() {
    this.service.getData(10, 1).subscribe(res => this.data = res);
  }

  updateData = (size, page) => {
    this.service.getData(size, page).subscribe(res => this.data = res);
  }
}
