import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @Input() data: object = [];
  @Input() changeHandler;

  size = 10;
  page = 1;

  response: object;

  constructor(private service: ApiService) {}

  ngOnInit() {
  }

  onSizeChange(size: string) {
    this.size = +size;
    this.changeHandler(this.size, this.page);
  }

  onPageChange(page: number) {
    const pageMap = {
      first: 1,
      prev: this.page - 1,
      next: this.page + 1,
    };

    this.page = pageMap[page];

    this.changeHandler(this.size, this.page);
  }

  onSubmit(id: number) {
    this.service.submitRecord(id, this.data[id]).then(res => this.response = res);
  }

}
