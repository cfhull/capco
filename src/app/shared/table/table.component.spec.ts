import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';
import { HttpClientModule } from '@angular/common/http';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableComponent ],
      imports: [HttpClientModule]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call change handler on size change', () => {
    component.changeHandler = () => null;
    spyOn(component, 'changeHandler');
    component.onSizeChange(1);
    expect(component.changeHandler).toHaveBeenCalled();
  });


  it('should call change handler on page change', () => {
    component.changeHandler = () => null;
    spyOn(component, 'changeHandler');
    component.onPageChange(1);
    expect(component.changeHandler).toHaveBeenCalled();
  });
});
