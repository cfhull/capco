import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  data: any = [];
  dataUrl = 'assets/data.json';
  constructor(private http: HttpClient) {
  }

  getData(size: number, page: number) {
    if (this.data.length === 0) {
      return this.http.get(this.dataUrl)
        .pipe(map(data => {
          this.data = data;
          return this.filterData(this.data, size, page);
        }));
    } else {
      return of(this.filterData(this.data, size, page));
    }
  }

  filterData(data: any, size: number, page: number) {
    const startIndex = size * page - size;
    return data.slice(startIndex, startIndex + size);
  }

  // will return not found since endpoint isn't set up
  submitRecord(id: number, recordData: object) {
    return new Promise(resolve => fetch('api/submit', {
      method: 'POST',
      body: JSON.stringify(Object.assign({}, recordData, {id})),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => resolve(res.statusText)));
  }
}

